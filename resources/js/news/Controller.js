export default class Controller {
  constructor(btnClass = '.btn__controller',
              btnActiveClass = 'btn__controller--active',
              badgeClass = '.item__content__image__badge',
              badgeActiveClass = 'item__content__image__badge--active') {

    this.btnClass = btnClass;
    this.btnActiveClass = btnActiveClass;
    this.badgeClass = badgeClass;
    this.badgeActiveClass = badgeActiveClass;
  }

  addListeners(buttons = this.btnClass, badges = this.badgeClass) {
    const btns = document.querySelectorAll(buttons);
    const bdgs = document.querySelectorAll(badges);
    btns.forEach(btn => btn.addEventListener(
      'click', e => this.sort(e)
    ));

    bdgs.forEach(badge => badge.addEventListener(
      'click', e => this.addToFavorites(e)
    ))
  }

  sort(e, btnActiveClass = this.btnActiveClass) {
    const items = document.querySelectorAll(`[data-tag *= ${e.target.id}]`);
    const btnClass = e.target.classList;
    const category = e.target.id;

    if (btnClass.contains(btnActiveClass)) {
      items.forEach(item => item.classList.remove(category))
    } else {
      items.forEach(item => item.classList.add(category))
    }
    btnClass.toggle(btnActiveClass);
  }

  addToFavorites(e) {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    const badgeId = e.target.dataset.id.replace('badge-', '');
    const item = document.querySelector(`[data-id *= item-${badgeId}]`);

    if (favorites.includes(badgeId)) {
      favorites.splice(favorites.indexOf(badgeId), 1);
      item.dataset.tag = item.dataset.tag.replace(' favorites', '');
      item.classList.remove('favorites');
    } else {
      favorites.push(badgeId);
      item.dataset.tag += ' favorites';
      item.classList.add('favorites');

    }
    e.target.classList.toggle(this.badgeActiveClass);

    localStorage.setItem('favorites', JSON.stringify(favorites));
  }

  initFavorites() {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    favorites.forEach(favoriteId => {
      const favoriteItem = document.querySelector(`[data-id *= item-${favoriteId}]`);
      favoriteItem.dataset.tag += ' favorites';
      favoriteItem.classList.add('favorites');
      document.querySelector(`[data-id *= badge-${favoriteId}]`).classList.add(this.badgeActiveClass);
    })
  }

  init() {
    if (document.querySelector(this.btnClass)) {
      this.addListeners();
      this.initFavorites();
    }
  }
}