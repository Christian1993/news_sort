export default class Item {
  constructor(controller, url = 'ex.json', wrapper = '.primary .row') {
    this.url = url;
    this.wrapper = wrapper;
    this.controller = controller;
    this.init();
  }

  makeItems(data, wrapper = this.wrapper) {
    let item = `
              <div class="col-sm-6 col-md-4 col-xl-3 item ${data.tags.join(' ')}" data-id="item-${data.id}"data-tag="${data.tags.join(' ')}">
              <div class="item__content">
                <div class="item__content__image" style="background-image: url('${data.imagePath}')" >
                    <span class="item__content__image__badge" data-id="badge-${data.id}"></span>
                </div>
                <div class="item__content__text">
                    <h3 class="item__content__text__title">${data.title}</h3>
                    <p class="item__content__text__tags">${data.tags.join(' ')}</p>
                    <p class="item__content__text__paragraph">${data.body.split(' ').slice(0, 20).join(' ')}...</p>
                </div>
                <div class="item__content__overlay">
                  <p class="item__content__overlay__text">
                    ${data.body}
                  </p>
                </div>
              </div>
            </div>
            `;
    document.querySelector(wrapper).innerHTML += item;
  }

  getData(url = this.url) {
    fetch(url)
      .then(response => response.json())
      .then(resp => resp.forEach(resp => this.makeItems(resp)))
      .then(() => this.controller.init())
      .catch(() => alert('Ups something gone wrong, please try later...'))
  }

  init(wrapper = this.wrapper) {
    if (document.querySelector(wrapper)) {
      this.getData();
    }
  }
}
