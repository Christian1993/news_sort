@extends('layouts.app')

@section('content')
    <h1>News</h1>
    <section class="controls">
        <button class="btn__controller btn__controller--active" id="sport">Sport</button>
        <button class="btn__controller btn__controller--active" id="news">News</button>
        <button class="btn__controller btn__controller--active" id="other">Other</button>
        <button class="btn__controller btn__controller--active" id="favorites">Favorites</button>
    </section>

    <section class="primary">
        <div class="row"></div>
    </section>
@endsection