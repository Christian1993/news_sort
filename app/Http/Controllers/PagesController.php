<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function index(){
        $title = "News Sort";
        return view('pages.index')->with('title', $title);
    }
}
