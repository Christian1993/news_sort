# Simple posts sorting app

#### Description:
You can hide or show posts by clicking the button with the right category. 
You can also add it to favorites. Posts added to favorites are stored in local storage, so 
they are avaible even after page is reloaded. Posts itself are fetched from json file and created on client side.

#### Technologies used:
* laravel, 
* javascript, 
* es6, 
* sass, 
* html, 
* bootstrap (only grid system), 
* json

#### How to run:
1. Download repo
2. In main directory type: _npm_ _install_
3. Type: _npm_ _run_ _dev_

<img src="https://drive.google.com/uc?export=view&id=1y0wBFghEqtgzYDPyzlMhv_8vz56dtdQB" width="600" height="auto">